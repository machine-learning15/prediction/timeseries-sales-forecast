# Time Series Sales Forecasting
![img/timeseries.png](img/timeseries.png)
**Introduction**

The ability to forecast data has great importance in a variety of fields such as supply chain operations, government funding and natural phenomena. 
This project is focused on applying several time series forecasting methods to retail sales data, consisting of weekly sales figures from a Walmart department store over a period of 143 weeks. 
Significant spikes in sales during holidays where some of these holidays don't fall on the same day of the calendar present a challenge for forecast models. 
This project investigates performance of Seasonal Trend Decomposition + Autoregressive Integrated Moving-Average (ARIMA) model to predict store sales for the next 4 weeks within 95% confidence interval. 
In addition, linear regression and regression trees are evaluated to forecast sales for each department. Comparative to ARIMA models, these models make it pretty straightforward to integrate external variables such as consumer price index and holiday indicator.

**Dataset**  
The dataset was provided by Walmart Inc, an American multinational retail corporation, for a 2014 data science competition (Kaggle). The dataset contains sales figures for 45 Walmart department stores in the United States. This project is focused on one store, identified by id 22.
Each sample has the following features: Departmental weekly sales, the date of the week's start day, a flag indicating whether the week contains a major holiday (Super Bowl, Labor Day, Thanksgiving, Christmas). Also consumer price index data is provided for each week. There is no publicly available test set with ground-truth values. Because of this, assessing selected best model's score must be done by making a submission to Kaggle's online platform. 


**Forecast Cost functions:**  
As a baseline model, naive seasonal forecast model is used, as the data showed strong 52 week seasonality, and it is easy to implement.

**Scaled MAE**, which scales the mean absolute error to average sales. 

```math
 MAE\,\% = \frac{\frac{1}{n}\sum_{t=0}^{n}\mid y_t-\hat{y_t}\mid}{\frac{1}{n}\sum_{t=0}^{n} y_t} = \frac{\sum_{t=0}^{n}\mid y-\hat{y}\mid}{\sum_{t=0}^{n} y}
```

**Weighted Mean Absolute Error**, which is adopted from the Kaggle competition's requirements. In this case, errors made for the holiday days have 5x weights.

```math
WMAE = \frac{1}{\sum w_t}\sum_{t=0}^{n} w_t \mid y_t- \hat{y_t}
```
- n is the number of rows
- $`\hat{y}_i`$ is the predicted sales
- $`y_i`$ is the actual sales
- $`w_i`$ are weights. w = 5 if the week is a holiday week, 1 otherwise

**Consumer Price Index**  
The sales are reported in USA dollars and without adjusting to CPI, sales figure contain the variation in inflation. To be able to identify and model the true trend and seasonality, sales figures are deflated.

```math
AdjustedSales_t = \frac{sales_t}{CPI_{t}}\times100
```

**Time series decomposition + ARIMA**  
This is a widely used approach to modeling time series data. Data is first decomposed into its trend, seasonality and remainder(also called noise/error/residual) components. Then ARIMA builds a model based on this remainder, and seasonality and trend components are added to the predictions. 
Decomposition method used here is the additive mode, as seasonal component of the data doesn't vary significantly with trend. Below graph is a sample graph showing this additive decomposition.
![img/decomposition.png](img/decomposition.png)

This model generates weekly store-wide sales forecast. Validation are done for  12 weeks horizon, and final forecast is generated for 4-weeks horizon.  

#### [Time series decomposition and benchmark notebook](StoreSalesTimeseriesAnalysis.ipynb)
#### [Model selection, diagnostics, and forecasting notebook](StoreSaleForecast-SARIMA.ipynb)

| Model                                 | Validation MAE % | Validation WMAE     |
|---------------------------------------|------------------|---------------------|
| Baseline: Seasonal naive forecast     | 3.64 %           |  23211              |
| Seasonal ARIMA                        | 3.11 %           |  17229              |

Below graph shows the actual values fall within the forecast confidence interval.

![img/sarima-validation.png](img/sarima-validation.png)

**Store Sales by Department**  

The features used are a combination of time-lagged sales figures for each department, holiday flag and CPI. Specifically, these models use t-52, t-3, t-2, t-1 sales figures to predict sales at time t.
Linear regression, random forest and Xgboost models are evaluated with parameterized random grid search, and linear regression model is selected based on the WMAE score.
Below graphs shows yearly sale volumes for each department.

![img/deparment-sales.png](img/deparment-sales.png)

#### [Exploratory data analysis, preprocessing and feature engineeering notebook](StoreSalesByDepartment.ipynb)
#### [Model selection, diagnostics and forecasting notebook](StoreSalesByDepartment-Model.ipynb)


| Model                                 | Validation MAE % | Validation WMAE   | Test MAE % | Test WMAE |
|---------------------------------------|------------------|-------------------|-------------|-----------|
| Baseline: Seasonal naive forecast     | 12.08 %          |  2075             |             |           |
| Linear Regression                     | 10.61 %          |  1770             | 1641        | 9.72 %    |



**Next Steps**  
Future work would involve fine-tuning parameters used for the machine learning models and generating forecasts for each of the store department combination to allow for online Kaggle competition submission. 

**References**  
- https://otexts.com/fpp2/
- http://people.duke.edu/~rnau/411home.htm
- https://machinelearningmastery.com
- [Data Science for Supply Chain Forecast](https://www.amazon.com/Data-Science-Supply-Chain-Forecast/dp/1730969437) by Nicolas Vandeput
- And many blogs and tutorials. 
