import pandas as pd
import itertools
import math
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os.path
import joblib
from dateutil import rrule
import plotly.express as px
from statsmodels.tsa.statespace.sarimax import SARIMAX
from sklearn.metrics import mean_squared_error

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
sns.set_theme(style="darkgrid")

## HELPER FUNCTIONS ##

def get_store_sales(path, store_id):
    """Get store by given id from sales dataset"""
    all_stores = pd.read_csv(path)
    store = all_stores[all_stores["Store"] == store_id]
    return store


def get_cpi(path, store_id):
    """Get CPI for the given store from cpi dataset"""
    cpi = pd.read_csv(path)[["Store", "Date", "CPI"]]
    cpi = cpi[cpi["Store"] == store_id]
    cpi.drop("Store", inplace=True, axis=1)
    return cpi


def get_department(store_df, department_id):
    """Get department by department id"""
    return store_df[store_df["Dept"] == department_id]


def drop_departments_with_missing_periods(store_df, departments, min_period):
    """This will drop all departments in the sales where there are missing periods"""
    drop_departments = []
    for d in departments:
        indx = get_department(store_df, d)
        if len(indx) < min_period:
            drop_departments.append(d)
    store_df = store_df[~store_df["Dept"].isin(drop_departments)]
    return store_df


def get_score_improvement(error1, error2):
    """Returns error improvement fraction:
    E.g: 2.86 to 2.66 -> 0.69 """
    return abs(error1 - error2) / max(error1, error2)


def check_duplicates(df):
    """Returns duplicate records if any, otherwise prints out 0."""
    series = df.duplicated()
    dup_count = series.sum()
    if dup_count == 0:
        return 0
    else:
        print("{} duplicate records, returning duplicate records".format(dup_count))
        return df[series]


def weeks_between(start_date, end_date):
    """Get number of weeks between 2 dates"""
    weeks = rrule.rrule(rrule.WEEKLY, dtstart=start_date, until=end_date)
    return weeks.count()


def standardize(df, column):
    """Creates a new column with standardized values of given column"""
    col_name = column + "_Std"
    df[col_name] = StandardScaler().fit_transform(np.array(df[column]).reshape(-1, 1))


def set_datetime_index(df, date_col, date_format):
    """Reformat date with given format, and sets dataframe's index to date"""
    df[date_col] = pd.to_datetime(df[date_col], format=date_format)
    df = df.set_index([date_col])
    return df


def reindex(df, freq="W-FRI"):
    """Reindex datetime index with given frequency"""
    new_index = pd.date_range(df.index.min(), df.index.max(), freq=freq)
    print("New Index = {}, existing index = {}".format(len(new_index), len(df.index.unique())))
    print("Setting index to new index")
    df = df.reindex(new_index, fill_value=0)
    return df


def get_monthly_store_sales(store_sales, store_id):
    """Resamples store sales to Monthly bins"""
    store = store_sales[store_sales["Store"] == store_id]
    store = set_datetime_index(df=store, date_col="Date", date_format="%d/%m/%Y")
    store = store.resample("M").sum()
    store["Store"] = store_id
    store = store.reset_index()
    store = store.set_index(["Date", "Store"]).unstack("Date").fillna(0)
    store.columns = store.columns.levels[1]
    return store


def get_all_monthly_store_sales_wide(store_sales):
    """Will resample monthly store sale data, then will create a wide format df where columns are date times"""
    store_ids = np.arange(1, 46)
    result = pd.DataFrame()
    for id in store_ids:
        store = get_monthly_store_sales(store_sales, id)
        result = result.append(store)

    return result


def convert_to_wide(df):
    """Each store is a column"""
    df = df.set_index(["Date", "Store"]).unstack("Store").fillna(0)
    df.columns = df.columns.levels[1].rename(None)
    return df


def get_partition_summary(series, partition_size=5):
    """Get mean and variance for each partition and plot"""
    parts = np.split(series, indices_or_sections=partition_size)
    result = pd.DataFrame(columns=["mean", "var"])
    for i, part in enumerate(parts, 1):
        result.loc[i] = [np.mean(part), np.var(part)]
    result.plot(subplots=True, title="Partition Summary Statistics");
    return result


def de_trend(series):
    """Models a linear regression line for given series.
    Returns model and trend"""
    from sklearn.linear_model import LinearRegression

    X = np.arange(1, len(series) + 1)
    X = X.reshape(len(X), -1)
    y = series.values
    print("X shape = {}, y shape = {}".format(X.shape, y.shape))
    model = LinearRegression().fit(X, y)
    trend = model.predict(X)
    print("Trend min = {}, Trend max = {}".format(min(trend), max(trend)))
    print("Trend range = ", max(trend) - min(trend))

    # plot
    detrended = y - trend
    plt.plot(y, label="y")
    plt.plot(trend, label="trend")
    plt.plot(detrended, label="detrended")
    plt.legend()
    plt.title("Trend model and Detrended series")
    plt.show()

    return model, trend


def plot_series(df, xlabel, ylabel, title):
    """Plot with plotly express"""
    fig = px.line(df, x=df.index, y=df.columns,
                  labels={
                      "index": xlabel,
                      "value": ylabel
                  },
                  title=title)
    fig.show()


def test_stationarity(series):
    """Uses Augmented Dickey-Fuller Test from stats.models
    Null hypothesis: Timeseries is non-stationary"""
    stationarity_test_result = adfuller(series, autolag="AIC")
    columns = ("adf", "p-value", "usedLag", "number of observations", "critical-values")
    test_values = stationarity_test_result[:5]

    dict(zip(columns, test_values))


def get_data(store_id=22):
    # store
    all_stores = pd.read_csv("data/sales-data-set.csv")
    store = all_stores[all_stores["Store"] == store_id]

    # holiday
    holidays = store[["Date", "IsHoliday"]]
    holidays = holidays.drop_duplicates()
    holidays = set_datetime_index(df=holidays, date_col="Date", date_format="%d/%m/%Y")
    holidays = reindex(holidays)

    # cpi
    cpi = pd.read_csv("data/features-data-set.csv")[["Store", "Date", "CPI"]]
    cpi = cpi[cpi["Store"] == store_id]
    cpi = set_datetime_index(df=cpi, date_col="Date", date_format="%d/%m/%Y")
    cpi = reindex(cpi)
    cpi.drop("Store", inplace=True, axis=1)
    cpi.index

    # # group sales by department
    store_sales = store.groupby(["Date", "Store"]).agg({"Weekly_Sales": "sum"}).reset_index()
    store_sales.drop("Store", inplace=True, axis=1)

    # datetime index
    store_sales = set_datetime_index(df=store_sales, date_col="Date", date_format="%d/%m/%Y")
    store_sales = reindex(store_sales)

    # merge holidays and store sales
    store_sales = store_sales.merge(holidays, how="left", left_index=True, right_index=True)
    store_sales["Week"] = store_sales.index.week
    store_sales["Month"] = store_sales.index.month

    # merge store and cpi
    store_sales = store_sales.merge(cpi, how="left", left_index=True, right_index=True)

    return store_sales


def adjust_cpi(store_sales):
    """Shift CPI so that prediction at t will use cpi t-1
    Calculate adjusted weekly sales so it is available for train and prediction"""
    lastweek_cpi = store_sales[["CPI"]]
    lastweek_cpi = lastweek_cpi.shift(1)
    lastweek_cpi.head()

    store_sales_processed = store_sales[["Weekly_Sales", "Week", "IsHoliday"]]
    store_sales_processed = store_sales_processed.merge(lastweek_cpi, how="left", left_index=True, right_index=True)
    store_sales_processed.dropna(inplace=True)

    store_sales_processed["Weekly_Sales_Adjusted"] = store_sales_processed["Weekly_Sales"] / store_sales_processed[
        "CPI"] * 100

    return store_sales_processed


def get_seasonal_diff(train_X):
    """Returns df with t and t-52, and their diff"""
    last_year = train_X[["Weekly_Sales"]].shift(52)
    last_year = pd.concat([last_year, train_X[["Weekly_Sales"]]], axis=1)
    last_year.columns = ["Weekly_Sales(t-52)", "Weekly_Sales"]
    last_year["SeasonalDiff"] = last_year["Weekly_Sales"] - last_year["Weekly_Sales(t-52)"]
    return last_year


def sarimax_model(train, test, order, seasonal_order):
    """Sarimax model with given order and seasonal order, model dignostics and forecast error"""
    sarimax = SARIMAX(train, order=order, seasonal_order=seasonal_order, enforce_stationarity=False,
                      enforce_invertibility=False)
    sarimax_fit = sarimax.fit(disp=False)
    print("Model Summary:")
    display(sarimax_fit.summary());
    #     print("Model bias:", sarimax_fit.resid.mean())
    print("Forecast Error: ")
    sarimax_forecast = sarimax_fit.get_forecast(len(test)).summary_frame(alpha=0.05)
    error = get_prediction_stats(test, sarimax_forecast[["mean"]])
    print("Model Diagnostics:")
    print()
    sarimax_fit.plot_diagnostics(figsize=(15, 9));
    return sarimax, error


def get_prediction_stats(test, predictions):
    """Returns Forecast Error dataframe"""
    import math

    assert len(test) == len(predictions)

    error = test[["Weekly_Sales"]]
    error["Predicted"] = predictions
    error["Error"] = error["Weekly_Sales"] - error["Predicted"]

    # Holidays
    error = pd.concat([error, test[["IsHoliday"]]], axis=1)
    error["Weight"] = error["IsHoliday"].map(lambda x: 5 if x else 1)
    error["Holiday_Weighted_Error"] = abs(error["Error"]) * error["Weight"]

    # MAE
    mae = abs(error["Error"]).mean()

    # MAE %
    mae_scaled = (mae / error["Weekly_Sales"].mean()) * 100

    # WMAE
    wmae = abs(error["Holiday_Weighted_Error"]).sum() / error["Weight"].sum()

    print("Average Sales :", error["Weekly_Sales"].mean())
    print("WMAE : ", wmae)
    print("MAE %: ", mae_scaled)

    plt.plot(error[["Weekly_Sales"]], label="observed")
    plt.plot(error[["Predicted"]], label="prediction")
    plt.xlabel("Date")
    plt.ylabel("Sales in constant dollars")
    plt.title("Predicted versus Observed Values")

    plt.legend();

    return error


def plot_acf_pacf(train_X, lags=60):
    """Plot ACF and PACF of given time series"""
    from statsmodels.tsa.stattools import acf, pacf

    # PLOT ACF
    acf = acf(train_X, nlags=lags)
    plt.stem(acf)
    plt.axhline(y=0, linestyle="-", color="black")
    plt.axhline(y=-1.96 / np.sqrt(len(train_X)), linestyle="--", color="gray")
    plt.axhline(y=1.96 / np.sqrt(len(train_X)), linestyle="--", color="gray")
    plt.xlabel("Lag")
    plt.ylabel("ACF")
    plt.show()

    # plot PACF
    pacf = pacf(train_X, nlags=lags, method="ols")
    plt.stem(pacf)
    plt.axhline(y=0, linestyle="-", color="black")
    plt.axhline(y=-1.96 / np.sqrt(len(train_X)), linestyle="--", color="gray")
    plt.axhline(y=1.96 / np.sqrt(len(train_X)), linestyle="--", color="gray")
    plt.xlabel("Lag")
    plt.ylabel("PACF");


def sarimax_gridsearch(train_X, test_X, p: list, d: list, q: list, P: list, D: list, Q: list, S: list):
    """Grid search sarimax parameters using given order and seasonal order parameter ranges.
    Returns best score and best config"""
    orders = list(itertools.product(p, d, q))
    seasonal_orders = list(itertools.product(P, D, Q, S))
    best_wmae, best_config = float("inf"), None

    for order in orders:
        for seasonal_order in seasonal_orders:
            try:
                srmx = SARIMAX(train_X, order=order, seasonal_order=seasonal_order, enforce_stationarity=False,
                               enforce_invertibility=False)
                srmx_fit = srmx.fit(ldisp=False)
                preds = srmx_fit.forecast(len(test_X))

                # calculate out-of-sample error
                rmse = math.sqrt(mean_squared_error(test_X, preds))
                error = get_prediction_stats(test_X, preds)
                wmae = abs(error["Holiday_Weighted_Error"]).sum() / error["Weight"].sum()
                if wmae < best_wmae:
                    best_wmae = wmae
                    best_config = [order, seasonal_order]
                print('ARIMA%s, %s WMAE=%.3f' % (order, seasonal_order, wmae))
            except Exception as e:
                print(e)
                continue

    print("Best WMAE: {}, Best config: {}".format(best_wmae, best_config))
    return best_config


def create_lookbacks(df, look_back=3, column_to_look=0):
    """Creates a 2D matrix of previous periods in the order of t-3, t-2, t-1 for example"""
    mtrx = df.values
    prev_periods_mtrx = np.full((len(mtrx), look_back), np.nan)
    for i in range(look_back, len(mtrx)):
        prev = mtrx[i - look_back:i, column_to_look]
        prev_periods_mtrx[i] = prev

    # generate columns
    columns = []
    for i in np.arange(look_back, 0, -1):
        columns.append("t-{}".format(i))

    return pd.DataFrame(prev_periods_mtrx, columns=columns)


def create_last_season(df, look_back=52, column_to_look=0):
    """Creates a 2D matrix of previous period t-52 for example"""
    mtrx = df.values
    prev_periods_mtrx = np.full((len(mtrx), 1), np.nan)
    for i in range(look_back, len(mtrx)):
        prev = mtrx[i - look_back, column_to_look]
        prev_periods_mtrx[i] = prev

    col_name = "t-{}".format(look_back)
    return pd.DataFrame(prev_periods_mtrx, columns=[col_name])


def get_correlation_with_target(df, target, method="pearson"):
    """Returns sorted correlations to target sorted by abs values"""
    corr = df.corr(method=method)[target].reset_index()
    corr["abs"] = abs(corr[target])
    corr.sort_values(by="abs", ascending=False, inplace=True)
    corr.drop(["abs"], inplace=True, axis=1)
    corr.columns = ["attr", "corr"]
    return corr


def encode_holidays(row):
    """Label encode holiday weeks."""
    if row["IsHoliday"] and row["Week"] == 6:
        return 1
    elif row["IsHoliday"] and row["Week"] == 36:
        return 2
    elif row["IsHoliday"] and row["Week"] == 47:
        return 3
    elif row["IsHoliday"] and row["Week"] == 52:
        return 4
    else:
        return 0


def save_dataset(dir_path, df, name):
    """Save dataframe to given dir"""

    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)
    df.to_csv(os.path.join(dir_path, name), index=True)


def save_results(dir_path, model, forecast):
    """Saves model and predictions"""
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

    # save model
    joblib.dump(model, os.path.join(dir_path, "model.joblib"))

    # save forecasts
    forecast.to_csv(os.path.join(dir_path, "forecast.csv"), index=True)
